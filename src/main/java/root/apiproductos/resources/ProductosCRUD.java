package root.apiproductos.resources;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.dao.ProductosJpaController;
import root.dao.exceptions.NonexistentEntityException;
import root.entity.Productos;


@Path("/productos")
public class ProductosCRUD 
{
    //Listar todos los productos
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response ListarProductos()
    {
        ProductosJpaController dao = new ProductosJpaController();
        List<Productos> listaProductos = dao.findProductosEntities();
        
        return Response.ok(200).entity(listaProductos).build();
    }
    
    //Buscar producto con el id
    @GET
    @Path("/{idproducto}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response BuscarProducto(@PathParam("idproducto") String idproducto)
    {
        ProductosJpaController dao = new ProductosJpaController();
        Productos producto = dao.findProductos(idproducto); 
        
        return Response.ok(200).entity(producto).build();
    }    
    
    //Crear producto
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response CrearProducto(Productos producto)
    {
        ProductosJpaController dao = new ProductosJpaController();
        
        try 
        {
            dao.create(producto);
        } 
        catch (Exception ex) 
        {
            Logger.getLogger(ProductosCRUD.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok(200).entity(producto).build();
    }   
    
    //Actualizar producto
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response ActualizarProducto(Productos producto)
    {
        ProductosJpaController dao = new ProductosJpaController();
        
        try
        {
            dao.edit(producto);
        } 
        catch (Exception ex) 
        {
            Logger.getLogger(ProductosCRUD.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok(200).entity(producto).build();
    }    
    
    //Eliminar producto con id
    @DELETE
    @Path("/{idproducto}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response EliminarProducto(@PathParam("idproducto") String idproducto)
    {
        ProductosJpaController dao = new ProductosJpaController();
        
        try 
        {
            dao.destroy(idproducto);
        }
        catch (NonexistentEntityException ex) 
        {
            Logger.getLogger(ProductosCRUD.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok("Producto eliminado").build();
    }    
}
