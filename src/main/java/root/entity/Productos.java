
package root.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "productos")
@NamedQueries({
    @NamedQuery(name = "Productos.findAll", query = "SELECT p FROM Productos p"),
    @NamedQuery(name = "Productos.findByIdproducto", query = "SELECT p FROM Productos p WHERE p.idproducto = :idproducto"),
    @NamedQuery(name = "Productos.findByProducto", query = "SELECT p FROM Productos p WHERE p.producto = :producto"),
    @NamedQuery(name = "Productos.findByPreciocompra", query = "SELECT p FROM Productos p WHERE p.preciocompra = :preciocompra"),
    @NamedQuery(name = "Productos.findByPrecioventa", query = "SELECT p FROM Productos p WHERE p.precioventa = :precioventa"),
    @NamedQuery(name = "Productos.findByStockactual", query = "SELECT p FROM Productos p WHERE p.stockactual = :stockactual")})
public class Productos implements Serializable 
{
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "idproducto")
    private String idproducto;
    @Size(max = 2147483647)
    @Column(name = "producto")
    private String producto;
    @Column(name = "preciocompra")
    private Integer preciocompra;
    @Column(name = "precioventa")
    private Integer precioventa;
    @Column(name = "stockactual")
    private Integer stockactual;

    public Productos() 
    {
    }

    public Productos(String idproducto) 
    {
        this.idproducto = idproducto;
    }

    public String getIdproducto() 
    {
        return idproducto;
    }

    public void setIdproducto(String idproducto) 
    {
        this.idproducto = idproducto;
    }

    public String getProducto() 
    {
        return producto;
    }

    public void setProducto(String producto) 
    {
        this.producto = producto;
    }

    public Integer getPreciocompra()
    {
        return preciocompra;
    }

    public void setPreciocompra(Integer preciocompra)
    {
        this.preciocompra = preciocompra;
    }

    public Integer getPrecioventa()
    {
        return precioventa;
    }

    public void setPrecioventa(Integer precioventa) 
    {
        this.precioventa = precioventa;
    }

    public Integer getStockactual() 
    {
        return stockactual;
    }

    public void setStockactual(Integer stockactual) 
    {
        this.stockactual = stockactual;
    }

    @Override
    public int hashCode() 
    {
        int hash = 0;
        hash += (idproducto != null ? idproducto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) 
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Productos)) 
        {
            return false;
        }
        Productos other = (Productos) object;
        if ((this.idproducto == null && other.idproducto != null) || (this.idproducto != null && !this.idproducto.equals(other.idproducto))) 
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString() 
    {
        return "root.entity.Productos[ idproducto=" + idproducto + " ]";
    }
    
}
