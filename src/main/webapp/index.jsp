<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Evaluación 3</title>
        
        <%--CDN Bootstrap 5 --%>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8" crossorigin="anonymous"></script>
     
    </head>
    <body>
            <div class="container-fluid">
              <div class="row">
                <div class="col-sm-12">
                    <h3>Api Productos</h3>   
                    <hr>
                    <table border="0" class="table table-striped table-sm">
                        <thead>
                            <tr>
                                <th scope="col">Método</th>
                                <th scope="col">Descripción</th>
                                <th scope="col">Url</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td scope="row">GET</td>
                                <td>Listar productos</td>
                                <td>https://wfevaluacionunidad3.herokuapp.com/api/productos/</td>
                            </tr>
                            <tr>
                                <td scope="row">GET</td>
                                <td>Lista producto por id</td>
                                <td>https://wfevaluacionunidad3.herokuapp.com/api/productos/{idproducto}</td>
                            </tr>
                            <tr>
                                <td scope="row">POST</td>
                                <td>Crear producto</td>
                                <td>https://wfevaluacionunidad3.herokuapp.com/api/productos/</td>
                            </tr>
                            <tr>
                                <td scope="row">PUT</td>
                                <td>Actualizar producto</td>
                                <td>https://wfevaluacionunidad3.herokuapp.com/api/productos/</td>
                            </tr>
                            <tr>
                                <td scope="row">DELETE</td>
                                <td>Eliminar producto por id</td>
                                <td>https://wfevaluacionunidad3.herokuapp.com/api/productos/{idproducto}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>                
            </div>
        </div>
    </body>
    
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.min.js" integrity="sha384-lpyLfhYuitXl2zRZ5Bn2fqnhNAKOAaM/0Kr9laMspuaMiZfGmfwRNFh8HlMy49eQ" crossorigin="anonymous"></script>     
</html>
